# Requirements

- backend: [User Api Express Repository](https://bitbucket.org/allanroberto18/user-api-express)
- docker
- react
- npm
- nginx

## Before start

Rename .env.dist to .env file, there you can check values to connect on backend api

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Create and run docker image

```
# Build the application
npm run build

# Create docker image
docker build -t IMAGE_NAME:IMAGE_TAG .

# Run docker container (port 80 for nginx)
docker run -d -p 80:80 --name=IMAGE_NAME --env-file=.env(.local|.dev.prod) IMAGE_NAME:IMAGE_TAG
```

### For docker on dev environment

1. [User Api Express Repository](https://bitbucket.org/allanroberto18/user-api-express) clone the repository and follow the instructions on README.md to create docker image for api (don't forget of database configurations);
2. On the docker-compose.yml make sure that image for api service is equal to the api image that was generated one step before;
3. Generate frontend docker image;
4. On the docker-compose.yml make sure that image for frontend service is equal to the frontend image that was generated one step before;
5. Run command below:
   ```
   docker-compose up 
   ```
6. Open [http://localhost](http://localhost)