FROM nginx

ENV API_HOST $API_HOST
ENV API_PORT $API_PORT

COPY ./build /usr/share/nginx/html

EXPOSE 80