import React, { Component } from 'react';
import { getAll } from '../../../provider/user';
import Table from 'react-bootstrap/Table';

export default class UserListComponent extends Component {

    constructor() {
        super();
        this.state = {
            users: []
        };
    }

    async componentDidMount() {
        getAll()
            .then(res => {
                this.setState({
                    users: res
                });
            }
        );
    }

    render() {
        return (
            <div className="col-12">
                <Table strip bordered hover responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.users.map(user => {
                            return (
                                <tr key={user._id}>
                                    <td>{user._id}</td>
                                    <td>{user.firstName}</td>
                                    <td>{user.lastName}</td>
                                    <td>{user.email}</td>
                                    <td></td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}