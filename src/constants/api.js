import env from "react-dotenv";

export const API_HOST = `http://${env.API_HOST}:${env.API_PORT}/api`;
export const API_USERS = `/users`;