import api from '../api';

export async function list(route) {
    const response = await api.get(route);
    const data = await response.data;

    return data;
}