import axios from 'axios';
import { API_HOST } from '../../constants/api'

export default axios.create({
    baseURL: API_HOST,
    responseType: "json"
})