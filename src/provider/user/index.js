import React, { Component } from 'react';
import { list } from '../repository';
import {API_USERS} from "../../constants/api";

export async function getAll() {
    const data = await list(API_USERS);

    return data;
}