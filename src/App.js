import UserListComponent from "./components/user/list";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <h1>User MS</h1>
        </div>
      </div>
        <div className="row">
          <UserListComponent />
        </div>
    </div>
  );
}

export default App;
